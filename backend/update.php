<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 3600');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

include_once('../config/database.php');
incluce_once('../objects/server.php');

$database = new Database();
$db = $database->getConnection();

$server = new Server($db);

$data = json_decode(file_get_contents('php://input'));

$server->id = $data->id;
$server->ip_address = $data->ip_address;
$server->port = $data->port;

if($server->update()) {
	echo('{');
		echo('"message": "Server was updated."');
	echo('}');
}else{
	echo('{');
		echo('"message": "Unable to update server."');
	echo('}');
}

?>