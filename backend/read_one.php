<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: access');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Credentials: true');
header('Content-Type: application/json');

include_once('../config/database.php');
include_once('../objects/server.php');

$database = new Database();
$db = $database->getConnection();

$server = new Server($db);
$server->id = isset($_GET['id']) ? intval($_GET['id']) : die();
$server->readOne();
$server_arr = array(
	'id' => $server->id,
	'ip_address' => $server->ip_address,
	'port' => $server->port,
	'user_nickname' => $server->user_nickname
);

echo(json_encode($server_arr));

?>