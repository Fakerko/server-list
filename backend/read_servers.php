<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json; charset=UTF-8');

include_once('./config/database.php');
include_once('./objects/server.php');
include_once('./sampQueryAPI.php');

$database = new Database();
$db = $database->getConnection();
$server = new Server($db);
$stmt = $server->read();
$num = $stmt->rowCount();

if ($num > 0) {
	$server_arr = array();
	$server_arr['records'] = array();

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);

		$status = new SampQueryAPI($ip_address, $port);
		$server_info = $status->getInfo();

		$server_item = array(
			'id' => $id,
			'ip_address' => $ip_address,
			'port' => $port,
			'user_nickname' => $user_nickname,
			'server_name' => $server_info['hostname'],
			'server_players' => $server_info['players'],
			'server_maxplayers' => $server_info['maxplayers']
		);
		array_push($server_arr['records'], $server_item);
	}
	echo(json_encode($server_arr));
}else{
	echo(json_encode(
		array('message' => 'No servers found.')
	));
}

?>