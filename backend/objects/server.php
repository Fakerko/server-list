<?php

class Server {

	private $db;
	private $table_name = 'servers';

	public $user_nickname;
	public $id;
	public $ip_address;
	public $port;
	public $created;

	public function __construct ($db) {
		$this->db = $db;
	}

	function read () {
		$query = 'SELECT 
					user_nickname, id, ip_address, port, created
				FROM 
					' . $this->table_name;
		
		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt;
	}

	function create () {
		$query = 'INSERT INTO
					' . $this->table_name . '
				SET
					ip_address=:ip_address, port=:port, user_nickname=:user_nickname, user_email=:user_email';
		$stmt = $this->db->prepare($query);

		$this->ip_address = htmlspecialchars(strip_tags($this->ip_address));
		$this->port = htmlspecialchars(strip_tags($this->port));
		$this->user_nickname = htmlspecialchars(strip_tags($this->user_nickname));
		$this->user_email = strip_tags($this->user_email);

		$stmt->bindParam(':ip_address', $this->ip_address);
		$stmt->bindParam(':port', $this->port);
		$stmt->bindParam(':user_nickname', $this->user_nickname);
		$stmt->bindParam(':user_email', $this->user_email);

		if($stmt->execute()) {
			return true;
		}

		return false;
	}

	function readOne () {
		$query = 'SELECT
					id, ip_address, port, user_nickname
				FROM
					' . $this->table_name . '
				WHERE
					id = ?
				LIMIT
					0,1';
		
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(1, $this->id);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$this->ip_address = $row['ip_address'];
		$this->port = $row['port'];
		$this->user_nickname = $row['user_nickname'];
	}

	function update () {
		$query = 'UPDATE
					' . $this->table_name . '
				SET 
					ip_address = :ip_address
					port = :port
				WHERE
					id = :id';
		
		$stmt = $this->db->prepare($query);

		$this->ip_address = htmlspecialchars(strip_tags($this->ip_address));
		$this->port = htmlspecialchars(strip_tags($this->port));
		$this->id = htmlspecialchars(strip_tags($this->id));

		$stmt->bindParam(':ip_address', $this->ip_address);
		$stmt->bindParam(':port', $this->port);
		$stmt->bindParam(1, $this->id);

		if($stmt->execute()) {
			return true;
		}

		return false;
	}

	function delete () {
		$query = 'DELETE FROM ' . $this->table_name . ' WHERE id = ?';

		$stmt = $this->db->prepare($query);

		$this->id = htmlspecialchars(strip_tags($this->id));

		$stmt->bindParam(1, $this->id);

		if($stmt->execute()) {
			return true;
		}

		return false;
	}
}

?>