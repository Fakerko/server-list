<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 3600');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

include_once('../config/database.php');
include_once('../objects/server.php');

$database = new Database();
$db = $database->getConnection();

$server = new Server($db);

$data = json_encode(file_get_contents('php://input'));

$server->id = $data->id;

if($server->delete()) {
	echo('{');
		echo('"message": "Server was deleted."');
	echo('}');
}else{
	echo('{');
		echo('"message": "Unable to delete server."');
	echo('}');
}

?>