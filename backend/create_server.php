<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json; charset=UTF-8');
header('Access-Control-Allow-Methods: POST');

include_once('./config/database.php');
include_once('./objects/server.php');

$database = new Database();
$db = $database->getConnection();

$server = new Server($db);

$data = json_decode(file_get_contents('php://input'));

$server->ip_address = $data->ip_address ? $data->ip_address : die();
$server->port = $data->port ? $data->port : die();
$server->user_nickname = $data->user_nickname ? $data->user_nickname : die();
$server->user_email = $data->user_email ? $data->user_email : die();

if($server->create()) {
	echo('{');
		echo('"message": "Server was added."');
	echo('}');
}else{
	echo('{');
		echo('"message": "Unable to add server."');
	echo('}');
}

?>