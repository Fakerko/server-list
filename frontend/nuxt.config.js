const pkg = require('./package')

const nodeExternals = require('webpack-node-externals')

module.exports = {
	mode: 'spa',

	/*
	** Headers of the page
	*/
	head: {
		title: pkg.name,
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: pkg.description }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			{ rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
		]
	},

	/*
	** Customize the progress-bar color
	*/
	loading: { color: '#FFFFFF' },

	/*
	** Global CSS
	*/
	css: [
		'vuetify/src/stylus/main.styl'
	],

	/*
	** Plugins to load before mounting the App
	*/
	plugins: [
		'~/plugins/vuetify',
		'~/plugins/axios',
		{ src: '~/plugins/auth', ssr: false }
	],

	/*
	** Nuxt.js modules
	*/
	modules: [
		// Doc: https://github.com/nuxt-community/axios-module#usage
		'@nuxtjs/axios',
		'@nuxtjs/auth',
		'@nuxtjs/toast'
	],
	/*
	** Axios module configuration
	*/
	axios: {
		// See https://github.com/nuxt-community/axios-module#options
	},

	toast: {
		position: 'top-right',
		duration: 2000
	},
	
	auth: {
		strategies: {
			facebook: {
				client_id: '532551227203738',
				userinfo_endpoint: 'https://graph.facebook.com/v2.12/me?fields=about,name,picture{url},email',
				scope: ['public_profile', 'email'],
				redirect_uri:'http://localhost:3000/fallback'
			},
		},
		redirect: {
			login: '/?login=1',
			logout: '/',
		}
	},

	/*
	** Build configuration
	*/
	build: {
		/*
		** You can extend webpack config here
		*/
		extractCSS: true,
		extend(config, ctx) {
		
			if (ctx.isServer) {
				config.externals = [
					nodeExternals({
						whitelist: [/^vuetify/]
					})
				]
			}
		}
	}
}
